from django.contrib import admin
from django import forms
from NIBL.models import NIBL

class Form(forms.ModelForm):
	class Meta:
		model = NIBL
		
class Admin(admin.ModelAdmin):
	form = Form
	list_display = ('order','email','bid', 'amt1','date')
	

admin.site.register(NIBL,Admin)