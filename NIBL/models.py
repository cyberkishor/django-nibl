from django.db import models
from django.conf import settings
from order.models import Order
'''

	<QueryDict: {u'EmailID': [u''], u'UID': [u'STEAM'], u'FirstName': [u'STEAM'], u'CustRefType': [u'162'], u'Mobile': [u''], u'LastName': [u'STEAM'], u'BID': [u'10439'], u'PREF': [u'162'], u'AMT1': [u'174.02'], u'Phone': [u''], u'Amount': [u'174.02'], u'BIDSYS': [u''], u'BankId': [u'004'], u'STFLG': [u'']}>
	loop
	EmailID 
	UID STEAM
	FirstName STEAM
	CustRefType 162
	Mobile 
	LastName STEAM
	BID 10439
	PREF 162
	AMT1 174.02
	Phone 
	Amount 174.02
	BIDSYS 
	BankId 004
	STFLG 

'''
class NIBL(models.Model):
	order = models.ForeignKey(Order)

	uid = models.CharField(max_length=255,null=True,blank=True)
	email = models.CharField(max_length=255,null=True,blank=True)
	first_name = models.CharField(max_length=255,null=True,blank=True)
	last_name = models.CharField(max_length=255,null=True,blank=True)
	mobile = models.CharField(max_length=255,null=True,blank=True)
	phone = models.CharField(max_length=255,null=True,blank=True)
	bankid = models.CharField(max_length=255,null=True,blank=True)
	
	CustRefType = models.CharField(max_length=255,null=True,blank=True)
	bid = models.CharField(max_length=255,null=True,blank=True)
	pref = models.CharField(max_length=255,null=True,blank=True)
	amt1 = models.DecimalField(max_digits=11,decimal_places=2)
	amount = models.DecimalField(max_digits=11,decimal_places=2)
	bidsys = models.CharField(max_length=255,null=True,blank=True)
	stflg = models.CharField(max_length=255,null=True,blank=True)


	date = models.DateTimeField(auto_now_add=True, help_text="HH:MM:SS DD Mmm YY, YYYY PST")
    

	def __unicode__(self):
		return ("%s") %(self.order.id)