from django import forms
from django.conf import settings
from django.utils.safestring import mark_safe
from paypal.standard.widgets import ValueHiddenInput
from NIBL.conf import (POSTBACK_ENDPOINT, SANDBOX_POSTBACK_ENDPOINT, RECEIVER_EMAIL)
from NIBL.conf import *
class NIBLPaymentForm(forms.Form):
	'''
	Defined Variables
	1. BankId  The bank ID of the respective Bank
	2. MD  Mode of Shopping Mall
	3. PID  Payee ID
	4. ITC  Item Code
	5. PRN  Payment Reference Number
	6. AMT  Amount of the payment
	7. CRN  Currency
	8. RU  Return URL from the Bank to your website (Shopping Mall) website. Please check the sample file (Response_post.aspx) attached for reference. 


	The required parameters are as follows.

	    - For static variables BankId, MD, PID, CRN, CG, USER_LANG_ID, UserType, AppType please leave these as it is.
	    - For variables PRN, ITC, AMT these are as per transaction. PRN and AMT is compulsory, ITC will appear in account statement.
	    - Variable RU is page where we send confirmation to you in your page that payment is done (currently this is also directed to our test server to response_post.aspx page).
	    - Additional variables might be defined if required
	'''
	
	BankId = forms.CharField(widget=ValueHiddenInput())
	MD = forms.CharField(widget=ValueHiddenInput())
	PID = forms.CharField(widget=ValueHiddenInput()) # payee id
	ITC = forms.CharField(widget=ValueHiddenInput()) # item code
	PRN = forms.CharField(widget=ValueHiddenInput()) # payment refrence number
	CG = forms.CharField(widget=ValueHiddenInput())
	USER_LANG_ID = forms.CharField(widget=ValueHiddenInput())
	UserType = forms.CharField(widget=ValueHiddenInput())
	AppType = forms.CharField(widget=ValueHiddenInput())
	
	AMT = forms.CharField(widget=ValueHiddenInput())
	CRN = forms.CharField(widget=ValueHiddenInput())
	RU = forms.CharField(widget=ValueHiddenInput())


	def __init__(self, button_type="buy", *args, **kwargs):
		super(NIBLPaymentForm, self).__init__(*args, **kwargs)
	   


	def render(self):
	    return mark_safe(u"""<form id="byitnow" action="%s" method="post" name="NIBLForm"> %s
	        <div class="buyitnow"><input type="image" src="%s" border="0" name="submit" alt="Buy it Now" /></div>
			</form>""" % (POSTBACK_ENDPOINT, self.as_p(), self.get_image()))
	    
	    
	def sandbox(self):
	    return mark_safe(u"""<form action="%s" method="post" name="NIBLForm"> %s 
	    	<input type="image" src="%s" border="0" name="submit" alt="Buy it Now" class="buyitnow"/>
			</form>""" % (SANDBOX_POSTBACK_ENDPOINT, self.as_p(), self.get_image()))
	 

	def get_image(self):
		return  IMAGE

   