'''
	nibl ipn manual configuration
	update order table 
	save order conformation details

	Note: catch django signals and update your order table

	return POST data:
	<QueryDict: {u'EmailID': [u''], u'UID': [u'STEAM'], 
	u'FirstName': [u'STEAM'], u'CustRefType': [u'172'], 
	u'Mobile': [u''], u'LastName': [u'STEAM'], u'BID': [u'10442'], 
	u'PREF': [u'172'], u'AMT1': [u'304.54'], u'Phone': [u''], 
	u'Amount': [u'304.54'], u'BIDSYS': [u''], 
	u'BankId': [u'004'], u'STFLG': [u'']}>

'''
from NIBL.models import NIBL
from urlparse import urlparse
import datetime
from django.conf import settings
def niblIPN(request):
	if request.method =='POST':
		nbl = NIBL(
				   order_id = request.POST['CustRefType'],
				   
				   uid = request.POST['UID'],
				   email = request.POST['EmailID'],
				   first_name = request.POST['FirstName'],
				   last_name = request.POST['LastName'],
				   mobile = request.POST['Mobile'],
				   phone = request.POST['Phone'],
				   bankid = request.POST['BankId'],
				   CustRefType = request.POST['CustRefType'],
				   bid = request.POST['BID'],
				   pref = request.POST['PREF'],
				   amt1 = request.POST['AMT1'],
				   amount = request.POST['Amount'],
				   bidsys =  request.POST['BIDSYS'],
				   stflg = request.POST['STFLG'],
				   )
			
		nbl.save()
			