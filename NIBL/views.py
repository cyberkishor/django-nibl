from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import redirect
from NIBL.ipn import niblIPN
@csrf_exempt
def process(request):
	if request.method == 'POST':
		niblIPN(request)
		return redirect('/order/complete/')
	else:
		return redirect('/order/cancel/')