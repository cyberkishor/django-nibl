from django.conf import settings

# API Endpoints.
POSTBACK_ENDPOINT = "http://<host_or_ip_provided_by_bank>/BankAwayRetail/sgonHttpHandler.aspx?Action.ShoppingMall.Login.Init=Y?"
SANDBOX_POSTBACK_ENDPOINT = "http://<host_or_ip_provided_by_bank>/BankAwayRetail/sgonHttpHandler.aspx?Action.ShoppingMall.Login.Init=Y?"
RECEIVER_EMAIL = '<receiver_email>'
# Images
IMAGE = getattr(settings, "nibl_image", "/static/images/nibl.png")
SUBSCRIPTION_IMAGE = "/static/images/nibl.png"
SANDBOX_IMAGE = getattr(settings, "nibl_sandbox_image", "/static/images/nibl.png")
SUBSCRIPTION_SANDBOX_IMAGE = "/static/images/nibl.png"

RETURN_URL = '/nibl/ipn/'

NIBLFRM = {	'BankId': '<BankId_given_by_babk>',
				'MD': '<',
				'PID': '<payment_id given_by_bannk>',
				'CRN': 'NPR',
				'CG': 'N',
				'USER_LANG_ID':'<USER_LANG_ID>',
				'UserType':'<UserType given_by_bannk>',
				'AppType':'<AppType given_by_bannk>',
			}